import { createContext, useContext, useMemo, useState } from "react";
import { ChildrenProp, ContextType, PostInfoProp } from "./types/post";
import { faker } from "@faker-js/faker";

function createRandomPost() {
    return {
      title: `${faker.hacker.adjective()} ${faker.hacker.noun()}`,
      body: faker.hacker.phrase(),
    };
  }


const PostContext = createContext<ContextType>({} as ContextType);

function PostProvider({children}: ChildrenProp) {
    const [posts, setPosts] = useState(() =>
    Array.from({ length: 30 }, () => createRandomPost())
  );
  const [searchQuery, setSearchQuery] = useState<string>("");
  

  // Derived state. These are the posts that will actually be displayed
  const searchedPosts =
    searchQuery.length > 0
      ? posts.filter((post) =>
          `${post.title} ${post.body}`
            .toLowerCase()
            .includes(searchQuery.toLowerCase())
        )
      : posts;

  function handleAddPost(post: PostInfoProp) {
    setPosts((posts) => [post, ...posts]);
  }

  function handleClearPosts() {
    setPosts([]);
  }

  const value = useMemo(() => {
    return {
      posts: searchedPosts,
      onAddPost: handleAddPost,
      onClearPosts: handleClearPosts,
      searchQuery,
      setSearchQuery,
    }
  }, [searchedPosts, searchQuery]);

  return(
    <PostContext.Provider value={value}>
        {children}
      </PostContext.Provider>
  )
}

function usePosts() {
    const context = useContext(PostContext)
    if(context === undefined)
    throw new Error("PostContext was used outside of the PostProvider");
    return context;
}


export {PostProvider, usePosts};