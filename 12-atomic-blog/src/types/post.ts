import { ReactNode } from "react";

export type PostInfoProp = {
        title: string;
        body: string;
}


export type ContextType = {
    posts: PostInfoProp[];
    onAddPost: (post: PostInfoProp) => void;
    onClearPosts: () => void;
    searchQuery: string;
    setSearchQuery: (query: string) => void;
}

export type ChildrenProp = {
    children: ReactNode;
}