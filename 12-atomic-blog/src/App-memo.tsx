import React, { FC, useState, useCallback, useEffect, useMemo, memo } from "react";
import { faker } from "@faker-js/faker";

interface Post {
  title: string;
  body: string;
}

function createRandomPost(): Post {
  return {
    title: `${faker.hacker.adjective()} ${faker.hacker.noun()}`,
    body: faker.hacker.phrase(),
  };
}

const App: FC = () => {
  const [posts, setPosts] = useState<Post[]>(() =>
    Array.from({ length: 30 }, () => createRandomPost())
  );
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [isFakeDark, setIsFakeDark] = useState<boolean>(false);

  const searchedPosts: Post[] =
    searchQuery.length > 0
      ? posts.filter((post) =>
          `${post.title} ${post.body}`
            .toLowerCase()
            .includes(searchQuery.toLowerCase())
        )
      : posts;

  const handleAddPost = useCallback(function handleAddPost(post: Post): void {
    setPosts((prevPosts) => [post, ...prevPosts]);
  }, []);

  const handleClearPosts = (): void => {
    setPosts([]);
  };

  useEffect(() => {
    document.documentElement.classList.toggle("fake-dark-mode");
  }, [isFakeDark]);

  const archiveOptions: { show: boolean; title: string } = useMemo(() => {
    return {
      show: false,
      title: `Post archive in addition to ${posts.length} main posts`,
    };
  }, [posts.length]);

  return (
    <section>
      <button
        onClick={() => setIsFakeDark((prevIsFakeDark) => !prevIsFakeDark)}
        className="btn-fake-dark-mode"
      >
        {isFakeDark ? "☀️" : "🌙"}
      </button>

      <Header
        posts={searchedPosts}
        onClearPosts={handleClearPosts}
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
      />
      <Main posts={searchedPosts} onAddPost={handleAddPost} />
      <Archive archiveOptions={archiveOptions} onAddPost={handleAddPost} />
      <Footer />
    </section>
  );
};

interface HeaderProps {
  posts: Post[];
  onClearPosts: () => void;
  searchQuery: string;
  setSearchQuery: (query: string) => void;
}

const Header: FC<HeaderProps> = ({
  posts,
  onClearPosts,
  searchQuery,
  setSearchQuery,
}) => {
  return (
    <header>
      <h1>
        <span>⚛️</span>The Atomic Blog
      </h1>
      <div>
        <Results posts={posts} />
        <SearchPosts
          searchQuery={searchQuery}
          setSearchQuery={setSearchQuery}
        />
        <button onClick={onClearPosts}>Clear posts</button>
      </div>
    </header>
  );
};

interface SearchPostsProps {
  searchQuery: string;
  setSearchQuery: (query: string) => void;
}

const SearchPosts: FC<SearchPostsProps> = ({
  searchQuery,
  setSearchQuery,
}) => {
  return (
    <input
      value={searchQuery}
      onChange={(e) => setSearchQuery(e.target.value)}
      placeholder="Search posts..."
    />
  );
};

interface ResultsProps {
  posts: Post[];
}

const Results: FC<ResultsProps> = ({ posts }) => {
  return <p>🚀 {posts.length} atomic posts found</p>;
};

interface MainProps {
  posts: Post[];
  onAddPost: (post: Post) => void;
}

const Main: FC<MainProps> = ({ posts, onAddPost }) => {
  return (
    <main>
      <FormAddPost onAddPost={onAddPost} />
      <Posts posts={posts} />
    </main>
  );
};

interface PostsProps {
  posts: Post[];
}

const Posts: FC<PostsProps> = ({ posts }) => {
  return (
    <section>
      <List posts={posts} />
    </section>
  );
};

interface FormAddPostProps {
  onAddPost: (post: Post) => void;
}

const FormAddPost: FC<FormAddPostProps> = ({ onAddPost }) => {
  const [title, setTitle] = useState<string>("");
  const [body, setBody] = useState<string>("");

  const handleSubmit = function (e: React.FormEvent<HTMLFormElement>): void {
    e.preventDefault();
    if (!body || !title) return;
    onAddPost({ title, body });
    setTitle("");
    setBody("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="Post title"
      />
      <textarea
        value={body}
        onChange={(e) => setBody(e.target.value)}
        placeholder="Post body"
      />
      <button>Add post</button>
    </form>
  );
};

interface ListProps {
  posts: Post[];
}

const List: FC<ListProps> = ({ posts }) => {
  return (
    <ul>
      {posts.map((post, i) => (
        <li key={i}>
          <h3>{post.title}</h3>
          <p>{post.body}</p>
        </li>
      ))}
    </ul>
  );
};

interface ArchiveProps {
  archiveOptions: { show: boolean; title: string };
  onAddPost: (post: Post) => void;
}

const Archive: FC<ArchiveProps> = memo(function Archive({
  archiveOptions,
  onAddPost,
}) {
  const [posts] = useState<Post[]>(() =>
    Array.from({ length: 10000 }, () => createRandomPost())
  );

  const [showArchive, setShowArchive] = useState<boolean>(
    archiveOptions.show
  );

  return (
    <aside>
      <h2>{archiveOptions.title}</h2>
      <button onClick={() => setShowArchive((prevShow) => !prevShow)}>
        {showArchive ? "Hide archive posts" : "Show archive posts"}
      </button>

      {showArchive && (
        <ul>
          {posts.map((post, i) => (
            <li key={i}>
              <p>
                <strong>{post.title}:</strong> {post.body}
              </p>
              <button onClick={() => onAddPost(post)}>Add as new post</button>
            </li>
          ))}
        </ul>
      )}
    </aside>
  );
});

const Footer: FC = () => {
  return <footer>&copy; by The Atomic Blog ✌️</footer>;
};

export default App;