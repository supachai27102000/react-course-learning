import { createContext, ReactNode, useReducer } from "react";
import { AuthAction, AuthActionType, AuthContextType, AuthReducerType } from "../types/types";

export const AuthContext = createContext<AuthContextType | null>(null)

type ChildrenProp = {
    children: ReactNode
}

const initialState: AuthReducerType = {
    user: null,
    isAuthenticated: false
}

function reducer(state: AuthReducerType, action: AuthAction) {
    switch (action.type) { 
        case AuthActionType.Login:
            return { ...state, user: action.payload, isAuthenticated: true };
        case AuthActionType.Logout:
            return { ...state, user: null, isAuthenticated: false };
        default:
            throw new Error("Unknown action");
    }
}

const FAKE_USER = {
  name: "Jack",
  email: "jack@example.com",
  password: "qwerty",
  avatar: "https://i.pravatar.cc/100?u=zz",
};

function AuthProvider({ children }: ChildrenProp) {
    const [{ user, isAuthenticated }, dispatch] = useReducer(
        reducer,
        initialState
    );


    function login(email: string, password: string) {
    if (email === FAKE_USER.email && password === FAKE_USER.password){
        dispatch({ type: AuthActionType.Login, payload: FAKE_USER });
    }}
    function logout() {
        dispatch({type: AuthActionType.Logout});
    }
    
    return( 
        <AuthContext.Provider value={{user, isAuthenticated, login, logout}}>
            {children}
        </AuthContext.Provider>
        );
}


export {AuthProvider}