import { createContext, ReactNode, useCallback, useEffect, useReducer } from "react";
import { CitiesContextType, CitiesReducerType, CitiesAction, CitiesActionType, ICityPost, ICity, } from "../types/types";

export const CitiesContext = createContext<CitiesContextType| null>(null)

type ChildrenProp = {
    children: ReactNode;
}

const BASE_URL = 'http://localhost:9000'

const initialState: CitiesReducerType = {
  cities: [],
  isLoading: false,
  currentCity: null,
  error: "",
}

function reducer(state: CitiesReducerType, action: CitiesAction) {
  switch(action.type) {
    case CitiesActionType.Loading:
      return {
        ...state,
        isLoading: true
      }

    case CitiesActionType.CitiesLoaded: 
      return {
        ...state,
        isLoading: false,
        cities: action.payload
      }

    case CitiesActionType.CityLoaded:
      return {
        ...state,
        isLoading: false,
        currentCity: action.payload
      };

    case CitiesActionType.CitiesCreated:
      return {
        ...state,
        isLoading: false,
        cities: [...state.cities, action.payload],
        currentCity: action.payload,
      }

    case CitiesActionType.CitiesDeleted:
      return {
        ...state,
        isLoading: false,
        cities: state.cities.filter((city) => city.id !== action.payload),
      }

    case CitiesActionType.Rejected:
      return{
        ...state,
        isLoading: false,
        error: action.payload
      }

    default:
      throw new Error("Unknown action type")
  }
}

function CitiesProvider({children}: ChildrenProp){
    const [{cities, isLoading, currentCity, error}, dispatch] = useReducer(
      reducer,
      initialState
      )
    // const [cities, setCities] = useState([])
    // const [isLoading, setIsLoading] = useState(false)
    // const [currentCity, setCurrentCity] = useState<Cities>({} as Cities)
  
    useEffect(function () {
      async function fetchCities(){
        dispatch({type: CitiesActionType.Loading})

        try{
        const res = await fetch(`${BASE_URL}/cities`);
        const data: ICity[] = await res.json();
        dispatch({type: CitiesActionType.CitiesLoaded, payload: data})
      } catch {
        dispatch({
           type: CitiesActionType.Rejected,
           payload: "There was an error loading data..."
          });
       }
      }
      fetchCities();
    }, []);


  const getCity = useCallback(async function getCity(id: string) {
    if (Number(id) === currentCity?.id) return;

    dispatch({ type: CitiesActionType.Loading })

    try {
      const res = await fetch(`${BASE_URL}/cities/${id}`);
      const data: ICity = await res.json();
      dispatch({ type: CitiesActionType.CityLoaded, payload: data });
    } catch {
      dispatch({
        type: CitiesActionType.Rejected,
        payload: "There was an error loading data..."
      });
    }
  }, [currentCity?.id]);
  
  async function createCity(newCity: ICityPost) {
      dispatch({type: CitiesActionType.Loading})
        try{
          const res = await fetch(`${BASE_URL}/cities`,
            {
              method: 'POST',
              body: JSON.stringify(newCity),
              headers: { 'Content-Type': 'application/json' }
            });
            const data: ICity = await res.json();
        
          dispatch({type: CitiesActionType.CitiesCreated, payload: data })

      } catch {
        dispatch({
          type: CitiesActionType.Rejected,
          payload: "There was an error creating city..."
         });
       }
  }
  
    async function deleteCity(id: number) {
      dispatch({type: CitiesActionType.Loading})

        try{
          await fetch(`${BASE_URL}/cities/${id}`, {
          method: "DELETE"
        })
        
        dispatch({
          type: CitiesActionType.CitiesDeleted,
          payload: id,
        })

      } catch {
        dispatch({
          type: CitiesActionType.Rejected,
          payload: "There was an error deleting city..."
         });
       }
  }
  

    return(
        <CitiesContext.Provider value={{
            cities,
            isLoading,
            currentCity,
            error,
            getCity,
            createCity,
            deleteCity
        }}>
            {children}
        </CitiesContext.Provider>
    )
}




export {CitiesProvider}