// Cities Context Types
export type CitiesContextType ={
    cities: ICity[];
    isLoading: boolean;
    currentCity: ICity | null;
    error: string;
    getCity(id: string): void;
    createCity(newCity: ICityPost): void;
    deleteCity(id: number): void;
}

// Auth Context Types
export type AuthContextType = {
    user: IUser | null;
    isAuthenticated: boolean;
    login(email: string, password: string): void;
    logout(): void;
  };

//Reducer Types

export type CitiesReducerType = {
    cities: ICity[];
    isLoading: boolean;
    currentCity: ICity | null;
    error: string;
  };

export type CitiesAction = 
| {
    type: CitiesActionType.Loading
  }
| {
    type: CitiesActionType.CitiesLoaded;
    payload: ICity[];
  }
| {
    type:CitiesActionType.CityLoaded;
    payload: ICity;
}
| {
    type: CitiesActionType.CitiesCreated;
    payload: ICity;
  }
| {
    type: CitiesActionType.CitiesDeleted;
    payload: number;
  }
| {
    type: CitiesActionType.Rejected;
    payload: string;
}

export enum CitiesActionType {
    Loading = 'loading',
    CitiesLoaded = 'cities/loaded',
    CitiesCreated = 'cities/created',
    CitiesDeleted = 'cities/deleted',
    Rejected = 'Rejected',
    CityLoaded = 'city/loaded'
}

export type AuthReducerType ={
    user: IUser | null;
    isAuthenticated: boolean;
}

export type AuthAction = {
    type: AuthActionType.Login;
    payload: IUser;
}
| {
    type: AuthActionType.Logout;
}




export enum AuthActionType {
    Login = 'login',
    Logout = 'logout',
}


// City/Country Types

export interface  ICity {
    cityName: string;
    country: string;
    emoji: string;
    date: string;
    notes: string;
    position: IPosition;
    id: number; 
}

export interface ICityPost {
    cityName: string;
    country: string;
    emoji: string;
    date: string;
    notes: string;
    position: IPosition;
}

// User Part
export interface ICountry {
    id: number;
    country: string;
    emoji: string;
}

export interface IPosition {
    lat: number;
    lng: number;
}
export interface IUser {
    name: string;
    email: string;
    password: string;
    avatar: string;
}

export type PositionArr = [lat: number, lng: number];


// UI
export enum ButtonType {
    Primary = 'primary',
    Back = 'back',
    Position = 'position',
}

// Form Country Fetch Data

export interface IFetchCountry{
    latitude: number;
    longitude: number;
    continent: string;
}

