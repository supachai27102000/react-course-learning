import { lazy, Suspense } from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

import { CitiesProvider } from './Context/CitiesContext';
import { AuthProvider } from './Context/FakeAuthContext';
import ProtectedRoute from './page/ProtectedRoute/ProtectedRoute';

import CityList from './components/CityList/CityList';
import CountriesList from './components/CountriesList/CountryList';
import City from './components/City/City';
import Form from './components/Form/Form';
import SpinnerFullPage from './components/SpinnerFullPage/SpinnerFullPage';

// import Product from './page/Product/Product';
// import Pricing from './page/Pricing/Pricing';
// import Login from './page/Login/Login';
// import Homepage from './page/Home/Homepage';
// import PageNotFound from './page/PageNotFound/PageNotFound';
// import AppLayout from './page/AppLayout/AppLayout';

const Homepage = lazy(() => import("./page/Home/Homepage"))
const Product = lazy(() => import("./page/Product/Product"))
const Pricing = lazy(() => import("./page/Pricing/Pricing"))
const Login = lazy(() => import("./page/Login/Login"))
const AppLayout = lazy(() => import("./page/AppLayout/AppLayout"))
const PageNotFound = lazy(() => import("./page/PageNotFound/PageNotFound"))







export default function App() {

  return (
    <AuthProvider>
      <CitiesProvider>
        <BrowserRouter>
        <Suspense fallback={<SpinnerFullPage/>}>
        <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="product" element={<Product />} />
        <Route path="pricing" element={<Pricing />} />
        <Route path="Login" element={<Login/>} />
        <Route 
          path="app" 
          element={
          <ProtectedRoute>
            <AppLayout />
          </ProtectedRoute>
          }
        >
        <Route index element={<Navigate replace to='cities' />}/>
        <Route path='cities' element={<CityList/>}/>
        <Route path='cities/:id' element={<City/>}/>
        <Route path='countries' element={<CountriesList/>}/>
        <Route path='form' element={<Form/>}/>
      </Route>
      
      <Route path="*" element={<PageNotFound/>} />
        </Routes>
      </Suspense>
    </BrowserRouter>
    </CitiesProvider>
    </AuthProvider>
  )
}
