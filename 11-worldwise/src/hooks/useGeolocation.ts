import { useState } from "react";

interface GeolocationPosition{
    lng: number;
    lat: number;
    defaultPosition: null
}

interface GeolocationError {
    message: string
}



export function useGeolocation(defaultPosition = null) {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [position, setPosition] = useState<GeolocationPosition| null>(defaultPosition);
    const [error, setError] = useState<GeolocationError | null>(null);
    function getPosition() {
      if (!navigator.geolocation){
        return setError({message: "Your browser does not support geolocation"});
        
    }
      setIsLoading(true);
      navigator.geolocation.getCurrentPosition(
        (pos) => {
          setPosition({
            lat: pos.coords.latitude,
            lng: pos.coords.longitude,
            defaultPosition: null
          });
          setIsLoading(false);
        },
        (error) => {
          setError({message: error.message});
          setIsLoading(false);
        }
      );
    }
    return { isLoading, position, error, getPosition };
  }