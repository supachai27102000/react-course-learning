import { useContext } from "react";
import { AuthContext } from "../Context/FakeAuthContext";

export function useAuth() {
    const context = useContext(AuthContext)

    if (!context)
        throw new Error("AuthContext was used outside AuthProvider");

    return context;
}

