import { useNavigate } from "react-router-dom";
import Button from "../Button/Button";
import { ButtonType } from "../../types/types";


export default function Backbutton() {
    const navigate = useNavigate();
    return (
        <Button
            type={ButtonType.Back}
            onClick={(e: { preventDefault: () => void; }) => {
          e.preventDefault();
          navigate(-1)}}>&larr; Back</Button>
    )
}
