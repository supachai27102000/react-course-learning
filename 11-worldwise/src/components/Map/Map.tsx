import { useEffect, useState } from 'react';
import { useNavigate} from 'react-router-dom';
import styles from './Map.module.css';
import {MapContainer,Marker,TileLayer,Popup, useMap, useMapEvents} from "react-leaflet";
import { useCities } from '../../hooks/useCities';
import { useGeolocation } from '../../hooks/useGeolocation';
import Button from '../Button/Button';
import { useUrlPosition } from '../../hooks/useUrlPosition';
import { ButtonType, PositionArr } from '../../types/types';




export default function Map() {
  
  const {cities} = useCities();

  const [mapPosition, setMapPosition] = useState<[number,number]>([40,0])

  

  const {isLoading: isLoadingPosition, position: geolocationPosition, getPosition} = useGeolocation();

  const [mapLat,mapLng] = useUrlPosition();

  useEffect(function(){
    setMapPosition([Number(mapLat),Number(mapLng)])
  },[mapLat, mapLng])

  useEffect(function() {
    if(geolocationPosition) setMapPosition([geolocationPosition.lat,geolocationPosition.lng])
  }, [geolocationPosition])

  return (
    <div className={styles.mapContainer}>
    {!geolocationPosition && (<Button type={ButtonType.Position} 
    onClick={getPosition}>
      {isLoadingPosition ? "Loading...": "Use your position"}
      </Button>)}
    <MapContainer
    center={mapPosition}
    zoom={13}
    scrollWheelZoom={false}
    className={styles.map}
    >
    <TileLayer
      attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
   {cities.map((city) => (<Marker position={[city.position.lat,city.position.lng]}  key={city.id}>
      <Popup>
        <span>{city.emoji}</span> <span>{city.cityName}</span>
      </Popup>
    </Marker>))}
    <ChangeCenter position={mapPosition}/>
    <DetectClick />
  </MapContainer>
    </div>
  )
}

function DetectClick() {
  const navigate = useNavigate();
  useMapEvents({
    click: (e) => 
    navigate(`form?lat=${e.latlng.lat}&lng=${e.latlng.lng}`),
  })
  return null
}

interface ChangeCenterProps{
  position: PositionArr
}

function ChangeCenter({position}:ChangeCenterProps){
  const map = useMap();
  map.setView(position);
  return null
}

