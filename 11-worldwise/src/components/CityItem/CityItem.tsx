import { Link } from 'react-router-dom';
import styles from './CityItem.module.css'
import { useCities } from '../../hooks/useCities';
import { formatDate } from '../../utils/utils';
import { ICity } from '../../types/types';

interface CityItemProps {
  city: ICity
}



export default function CityItem({city}: CityItemProps) {
  const { currentCity, deleteCity } = useCities()
  const { cityName, emoji, date, id, position } = city
  
  function handleClick(e: React.MouseEvent<HTMLButtonElement,MouseEvent>) {
  e.preventDefault();
    deleteCity(id);
  }
  return (
    <li>
      <Link className={
        `${styles.cityItem}
         ${id === currentCity?.id ? styles['cityItem--active']: ""}`} to={`${id}?lat=${position.lat}&lng=${position.lng}`}>
      <span className={styles.emoji}>{emoji}</span>
      <h3 className={styles.name}>{cityName}</h3>
      <time className={styles.date}>{formatDate(date)}</time>
      <button className={styles.deleteBtn} onClick={handleClick}>&times;</button>
      </Link>
    </li>
  )
}
