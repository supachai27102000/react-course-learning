import styles from './CountryList.module.css';

import { useCities } from '../../hooks/useCities';
import { ICity, ICountry } from '../../types/types';


import Spinner from '../Spinner/Spinner';
import CountryItem from '../CountryItem/CountryItem';
import Message from '../Message/Message';



function CountryList(){

  const {cities , isLoading } = useCities()


  if (isLoading) return <Spinner />;

  if (!cities.length) {
    return <Message message='Add your first country by a city on the map' />;
  }

  const countries = cities.reduce(
    (arr: ICountry[], currentValue: ICity): ICountry[] => {
        if(!arr.map(city => city.country).includes(currentValue.country))
        return [
          ...arr,
          {id: currentValue.id, country: currentValue.country, emoji: currentValue.emoji},
      ]
      else return arr
    },
    [],
);

  return (
    <ul className={styles.countriesList}>
      {countries.map((country) => (
        <CountryItem country={country} key={country.id} countrys={''} />
      ))}
    </ul>
  );
}

export default CountryList;