import React,{ ReactNode } from 'react'
import styles from './Button.module.css'
import { ButtonType } from '../../types/types';

type ButtonProp = {
  children: ReactNode;
  onClick?(e: React.MouseEvent<HTMLButtonElement,MouseEvent>): void;
  type: ButtonType;
}

export default function Button({children,onClick, type}:ButtonProp) {
  return (
    <button onClick={onClick} className={`${styles.btn} ${styles[type]}`}>
      {children}
    </button>
  )
}
