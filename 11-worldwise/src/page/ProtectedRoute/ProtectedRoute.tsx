import { ReactNode, useEffect } from 'react'
import { useAuth } from '../../hooks/useAuth'
import { useNavigate } from 'react-router-dom';



export default function ProtectedRoute({children}:{children: ReactNode}) {
    const {isAuthenticated} = useAuth();
    const navigate = useNavigate();

  useEffect(function(){
    if(!isAuthenticated) navigate('/')
  },
    [isAuthenticated, navigate]
);

  return ( isAuthenticated ? children: null );
}
