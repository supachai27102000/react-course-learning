import React,{ useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../../hooks/useAuth";
import { ButtonType } from "../../types/types";

import styles from "./Login.module.css"
import PageNav from "../../components/navigation/PageNav";
import Button from "../../components/Button/Button";




export default function Login() {
  const { isAuthenticated, login } = useAuth();
  // PRE-FILL FOR DEV PURPOSES
  const navigate = useNavigate()
  const [email, setEmail] = useState("jack@example.com");
  const [password, setPassword] = useState("qwerty");

  useEffect(function(){
    document.title = 'Login | Worldwise';

    return () => {document.title = 'Worldwise'}
  },[])
  

  useEffect(function(){
    if(isAuthenticated){
      navigate('/app',{replace: true})
    }
  },[isAuthenticated,navigate]);

  function handleSubmit(e: React.FormEvent<HTMLFormElement>){
    e.preventDefault();

    if(!email || !password) return;

    login(email,password);
  }

  return (
    <main className={styles.login}>
      <PageNav/>
      <form className={styles.form} onSubmit={handleSubmit}>
        <div className={styles.row}>
          <label htmlFor="email">Email address</label>
          <input
            type="email"
            id="email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
          />
        </div>

        <div className={styles.row}>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
        </div>

        <div>
          <Button type={ButtonType.Primary}>Login</Button>
        </div>
      </form>
    </main>
  );
}