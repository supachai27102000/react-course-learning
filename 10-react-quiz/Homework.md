Tasks:

- Review data flow and passed props
- Identify prop drilling problem
- Use the Context API to fix the (very small) prop drilling problem
- Create a new context `QuizContext` with the reducer we created earlier
- Create a custom provider component `QuizProvider` and provide all the state to the app
- Creat a custom hook to consume state all over the application
- Delete all unnecessary props
- IMPORTANT: Note how you actually need state right in App component. This mean you need to wrap the App into the context (HINT: try in index.tsx)