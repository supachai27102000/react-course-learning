export enum TimeReducerAction  {
    TIMER
}

export interface TimeReducerType {
    type: TimeReducerAction;
    payload?: string;
}

export enum SoundReducerAction  {
    ALLOWED_SOUND
}

export interface SoundReducerType {
    type: SoundReducerAction;
    payload?: string;
}